package com.kaigejava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnnotionStudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnnotionStudyApplication.class, args);
	}

}
