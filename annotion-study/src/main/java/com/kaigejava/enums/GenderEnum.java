package com.kaigejava.enums;

/**
 * Created by kaigejava on 2019/6/16.
 * 性别枚举类
 */
public enum GenderEnum {
    MAN(1,"男"),
    WMAN(0,"女");

    private Integer code;
    private String description;

    GenderEnum(){}
    GenderEnum(int code, String description){
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }


    public static String getDescriptionByCode(Integer code ){
        if (null != code) {
            for(GenderEnum type : GenderEnum.values()){
                if(type.getCode() == code){
                    return type.getDescription();
                }
            }
        }
        return null;
    }


}
