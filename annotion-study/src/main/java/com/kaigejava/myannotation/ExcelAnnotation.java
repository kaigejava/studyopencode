package com.kaigejava.myannotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by kaigejava on 2019/6/16.
 * Excel导出注解
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelAnnotation {

    /**
     * 导出文件的中文名称
     */
    String ZHName() ;

    /**
     * 导出对应的列
     * A、B、C...
     * @return
     */
    String colum();


    /**
     * 提示信息
     * @return
     */
    String Tips() default  "";

    /**
     * 提示信息
     */
    public abstract String prompt() default "";

    /**
     * 设置只能选择不能输入的列内容.
     */
    public abstract String[] combo() default {};

    /**
     *   是否导出数据,应对需求:有时我们需要导出一份模板,这是标题需要但内容需要用户手工填写.
     * @return
     */
    public abstract boolean isExport() default true;

    /**
     * 数字与文字转换
     */
    public abstract String value() default "";
    /**
     * 前缀
     */
    public abstract String pre() default "";
    /**
     * 后缀
     * @return
     */
    public abstract String suffix() default "";
    /**
     * 是否需要格式化
     */
    public abstract boolean isFormat() default false;

    /**
     * 是否需要格式化日期.
     * 改字段必须注解在date类型的字段上。否则会有问题
     * @return
     */
    public abstract boolean isDateFormat() default false;

    /**
     * 日期格式化 format
     * @return
     */
    public abstract String dateFormatSdf() default "yyyy-MM-dd";


    /**
     * 枚举类的类
     * @return
     */
    public abstract Class EnumClazz() default String.class;

    /**
     *枚举类的方法
     * @return
     */
    public abstract String EnumMethodName() default  "getDescriptionByCode";


    /**
     * 枚举类的方法的参数
     * @return
     */
    public  abstract  Class methodArgClass() default Integer.class;

    /**
     * 声明枚举类解析对象
     * @return
     */
    public abstract  EnumCode2Description[] EnumCode2Description() default {};


    /**
     * 枚举类 code转 description
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target( {})
    public @interface EnumCode2Description{

        /**
         *  调用枚举列的方法名称
         * @return
         */
        public abstract String methodName();

        /**
         * 调用枚举类方法的参数
         * @return
         */
        public  abstract  Class methodArgClass() default Integer.class;

        /**
         * 枚举类
         * @return
         */
        public  abstract  Class clazz();

    }

    /**
     * 声明解析日期封装对象
     * @return
     */
    public abstract  myDateFormat2Pattern[] myDateFormat2Pattern() default {};
    /**
     * 日期格式化-封装注解
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target({})
    public @interface  myDateFormat2Pattern{
        public abstract  String pattern() ;
        public  abstract  boolean isDateFormate() ;
    }


}
