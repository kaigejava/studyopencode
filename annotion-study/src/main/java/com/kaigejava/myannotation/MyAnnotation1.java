package com.kaigejava.myannotation;

import java.lang.annotation.*;

/**
 * Created by kaigejava on 2019/6/15.
 * 这是第一个自定义注解
 * 注解需要使用到元注解
 * 元注解有4个：@Target、@Retention、@Inherited、@Documented
 */
//target:这个注解的作用域。
    //作用域包括：构造器、字段、局部变量、方法、包、参数、类或接口。
    //如果多个，使用英文的逗号分隔：ElementType.METHOD，ElementType.FIELD
@Target({ElementType.METHOD, ElementType.FIELD})
//retention:这个注解的生命周期
//生命周期有：只在源码中显示的、class和源码都有的、运行时候存在的
@Retention(RetentionPolicy.RUNTIME)
//标识性的元注解。标识当前的注解可以有子注解继承的
@Inherited
//生成doc文档的时候
@Documented()
public @interface MyAnnotation1 {

    /**
     * 如果只有一个成员的时候，名字必须娇value.
     * 在使用的时候，等号也可以省略的
     * @return
     */
    public abstract String value();

    /**
     * int类型
     */
    public abstract  int age() default  0;

    /**
     * class类型
     */
    public abstract Class getObj() default  String.class;

    /**
     * 枚举类型
     */
    enum Level { BAD, INDIFFERENT, GOOD };
    Level getLevelValue() default  Level.BAD;

    /**
     * String类型数组
     */
    public abstract String []  getArr() default  {};
}
