package com.kaigejava.pojo;

import com.kaigejava.enums.GenderEnum;
import com.kaigejava.myannotation.ExcelAnnotation;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * Created by kaigejava on 2019/6/16.
 * excel导出对象
 */
@Data
public class AnnotationUserPoJo implements Serializable {

    /**
     * 主键id
     */
    @ExcelAnnotation(ZHName = "主键ID", colum ="A" )
    private Integer id;

    /**
     * 姓名
     */
    @ExcelAnnotation(ZHName = "姓名", colum ="B" )
    private String name;

    /**
     * 年龄
     */
    private int age;

    /**
     * 性别
     */
    //    @ExcelAnnotation(ZHName = "性别", colum ="D")
 /*  @ExcelAnnotation(ZHName = "性别", colum ="D", EnumClazz=GenderEnum.class,
           EnumMethodName = "getDescriptionByCode" ,methodArgClass=Integer.class )
*/

@ExcelAnnotation(ZHName = "性别",colum = "D",
         EnumCode2Description = {@ExcelAnnotation.EnumCode2Description(
                 clazz =GenderEnum.class,methodName = "getDescriptionByCode",methodArgClass = Integer.class)})

 private Integer Gender;


    /**
     * 生日
     */
    //    @ExcelAnnotation(ZHName = "生日", colum ="C")
  //  @ExcelAnnotation(ZHName = "生日", colum ="C" ,isDateFormat = true,dateFormatSdf = "yyyy-MM-dd")
@ExcelAnnotation(ZHName = "生日",colum = "C"
        ,myDateFormat2Pattern = {@ExcelAnnotation.myDateFormat2Pattern(isDateFormate = true,pattern = "yyyy-MM-dd")})
    private Date birthDay;



}
