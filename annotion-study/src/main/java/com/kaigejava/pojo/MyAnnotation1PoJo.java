package com.kaigejava.pojo;


import com.kaigejava.myannotation.MyAnnotation1;

/**
 * Created by kaigejava on 2019/6/15.
 * MyAnnotation1注解的pojo对象
 */
public class MyAnnotation1PoJo {

    @MyAnnotation1("方法上面的kaigejava")
    public String getMoth(){
        return "方法上面的";
    }
}
