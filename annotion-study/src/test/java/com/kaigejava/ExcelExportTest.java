package com.kaigejava;

import com.kaigejava.enums.GenderEnum;
import com.kaigejava.pojo.AnnotationUserPoJo;
import com.kaigejava.utils.AnnotationExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by kaigejava on 2019/6/16.
 */
@Slf4j
public class ExcelExportTest {

    @Test
    public void exportExcelTest() throws  Exception{
        //这里模拟数据库查询到的数据
        List<AnnotationUserPoJo> dataList = new ArrayList<AnnotationUserPoJo>();

        AnnotationUserPoJo pojo1 = new AnnotationUserPoJo();
        pojo1.setAge(10);
        pojo1.setBirthDay(new Date());
        pojo1.setGender(GenderEnum.WMAN.getCode());
        pojo1.setId(1);
        pojo1.setName("kaigejava");

        AnnotationUserPoJo pojo2 = new AnnotationUserPoJo();
        pojo2.setAge(11);
        pojo2.setBirthDay(new SimpleDateFormat("yyyy-mm-dd").parse("2018-09-12"));
        pojo2.setGender(GenderEnum.MAN.getCode());
        pojo2.setId(1000);
        pojo2.setName("凯哥Java");
        dataList.add(pojo1);
        dataList.add(pojo2);
        //导出流
        //OutputStream output = new FileOutputStream(new File("D:\\var"));
        FileOutputStream output = new FileOutputStream("D:\\人员表.xls");
        AnnotationExcelUtils<AnnotationUserPoJo> excelUtils = new AnnotationExcelUtils<>(AnnotationUserPoJo.class);
        excelUtils.exportExcelEnhance(dataList,"人员表",output);
        log.info("====>>用户信息导出完毕。。。。。");
    }

    public void deprecatedTest(){
        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
    }
}
