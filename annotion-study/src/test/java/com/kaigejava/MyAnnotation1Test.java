package com.kaigejava;



import com.kaigejava.myannotation.MyAnnotation1;

import java.lang.reflect.Method;

/**
 * Created by kaigejava on 2019/6/15.
 * 自定义注解的使用
 * 通过反射获取
 */
public class MyAnnotation1Test {

    public static void main(String[] args) {
        try {
          //1:使用类加载器加载类
            Class clazz = Class.forName("com.kaigejava.pojo.MyAnnotation1PoJo");
            //2：在对于的类上是否存在对于注解
            //获取方法上是否有注解
            for(Method method : clazz.getMethods()){
                boolean isMothExistMyAnnotation1 = method.isAnnotationPresent(MyAnnotation1.class);
                if(isMothExistMyAnnotation1){
                    //3：如果存在。就获取
                    MyAnnotation1 myAnnotation1 = (MyAnnotation1)method.getDeclaredAnnotation(MyAnnotation1.class);
                    System.out.println("==>:方法"+method.getName()+"有注解。值为："+myAnnotation1.value());

                }else{
                    System.out.println("==>:方法"+method.getName()+"无注解");
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
