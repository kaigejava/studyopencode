package com.kaigejava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReflectStudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReflectStudyApplication.class, args);
	}

}
