package com.kaigejava.domain;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by kaigejava on 2019/6/22.
 * webset:www.kaigejava.com
 * 需要说明：
 *  类需要实现序列化
 */
@Data
@ToString
public class Person  implements Serializable{

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private  Integer age;
}
