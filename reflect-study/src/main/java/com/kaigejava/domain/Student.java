package com.kaigejava.domain;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by kaigejava on 2019/6/22.
 * website:www.kaigejava.com
 */
@Data
public class Student  extends  Person implements Serializable {

    public Student(){
    }
    public Student(String schoolName){
        this.schoolName = schoolName;
    }

    /**
     * 学校名称
     */
    private String  schoolName;

    /**
     * 地址
     */
    public String addr;


    /**
     * 真实名称
     */
    public String realName;


    public String toString(){
        return "name="+this.getName()+"age="+this.getAge()+"schoolName="+this.getSchoolName();
    }
}
