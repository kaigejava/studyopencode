package com.kaigejava.service.impl;

import com.kaigejava.domain.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaigejava on 2019/6/23.
 */
public class PersonServiceImpl {

    public List<Person> getList(){
        List<Person> list = new ArrayList<>();
        Person person1 = new Person();
        person1.setAge(18);
        person1.setName("sixiaosi");

        Person person2 = new Person();
        person2.setAge(19);
        person2.setName("kaigejava");
        list.add(person1);
        list.add(person2);
        return list;
    }

    public Person getPersonByName(String name){
        List<Person> list = this.getList();
        for (Person person:list){
            if(name.equals(person.getName())){
                return  person;
            }else{
                return  null;
            }
        }
        return  null;
    }
}
