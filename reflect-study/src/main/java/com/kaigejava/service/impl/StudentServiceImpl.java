package com.kaigejava.service.impl;


import com.kaigejava.domain.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaigejava on 2019/6/23.
 * 学生对象service实现类
 */
public class StudentServiceImpl  {

    public List<Student> getList(){
        List<Student> list = new ArrayList<>();
        Student student1 = new Student();
        student1.setSchoolName("北京大学附属中学");
        student1.setName("kaige");
        student1.setAge(20);
        student1.setAddr("北京");
        student1.setRealName("凯哥Java");

        Student student2 = new Student();
        student2.setSchoolName("清华大学附属中学");
        student2.setName("sixiaosi");
        student2.setAge(19);
        student2.setAddr("上海");
        student2.setRealName("司小司");

        list.add(student1);
        list.add(student2);
        return  list;
    }

}
