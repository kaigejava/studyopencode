package com.kaigejava.utils;

import java.util.Map;

/**
 * Created by kaigejava on 2019/6/23.
 */
public class ContextBeanUtils<T>  {
    Class<T> clazz;
    Map<String ,Object> context1 = ContextMapUtil.getContext();

    public    T getBean(String beanName){
        T entity = null;
        try {
            if(null != context1){
                String classAllName = (String)context1.get(beanName);
                //通过反射来得到对象
                clazz = (Class<T>) Class.forName(classAllName);
                entity = (T)(entity == null ? clazz.newInstance() : entity);// 如果不存在实例则新建.

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return  entity;
    }


    public    T getBean(Map<String ,Object> context ,String beanName){
        T entity = null;
        try {
            if(null != context){
                String classAllName = (String)context.get(beanName);
                //通过反射来得到对象
                clazz = (Class<T>) Class.forName(classAllName);
                entity = (T)(entity == null ? clazz.newInstance() : entity);// 如果不存在实例则新建.

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return  entity;
    }
}
