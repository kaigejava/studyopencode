package com.kaigejava.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kaigejava on 2019/6/23.
 * 模拟 spring ApplicationContext的map工具类
 */
public class ContextMapUtil  {

    private static final Map<String,Object>  context = new HashMap<>();

    public static  Map<String,Object> getContext(){
        return  context;
    }

}
