package com.kaigejava.utils;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by kaigejava on 2019/6/23.
 */
public class XmlUtils {

    public static  Map<String,Object>  getBeanMap(String fileName) {
        try {
            SAXReader reader = new SAXReader();
            InputStream is =
                    XmlUtils.class.getClassLoader().getResourceAsStream(fileName);
            Document doc = reader.read(is);
            Element root = doc.getRootElement();
            Iterator<Element> it = root.elementIterator();
            Map<String,Object> context = ContextMapUtil.getContext();
            while(it.hasNext()){
                Element e = it.next();//获取子元素
                // 获取元素
                Element idElement = e.element("id");
                String id =  idElement.getText();
                System.out.println(id);
                Element nameElement = e.element("all-class");
                //获取元素中的文本内容
                String className = nameElement.getText();
                System.out.println(className); //
                context.put(id,className);
            }
            return context;
        }catch (Exception e){
            e.printStackTrace();
        }
        return  null;
    }
}
