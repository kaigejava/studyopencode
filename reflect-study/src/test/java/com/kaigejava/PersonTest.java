package com.kaigejava;

import com.kaigejava.domain.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by kaigejava on 2019/6/22.
 * website:www.kaigejava.com
 */
@Slf4j
public class PersonTest {

    @Test
    public  void newPersonTest(){
        Person p = new Person();
        p.setName("凯哥Java");
        p.setAge(25);
        log.info("==>p:{}",p.toString());
    }
}
