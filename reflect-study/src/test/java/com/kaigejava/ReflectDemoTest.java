package com.kaigejava;

import com.kaigejava.domain.Person;
import com.kaigejava.domain.Student;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by kaigejava on 2019/6/22.
 * website:www.kaigejava.com
 * java 反射API演示demo类
 */
@Slf4j
public class ReflectDemoTest {

    /**
     * 1:通过反射获取到对象的全包名
     */
    @Test
    public void getPackageTest() throws  Exception{
        Person person = new Person();
        log.info("person对象的全包名为:{}",person.getClass().toString());
        log.info("person class getName:{}",person.getClass().getName());
    }


    /**
     * 2：class对象的几种获取方式
     */
    @Test
    public void getClassTest() throws  Exception{
        //1：class.fromName获取
        Class<?> clazz1 = null;
        clazz1 = Class.forName("com.kaigejava.domain.Person");

        //2：通过new关键之创建的类调用其getClass方法
        Class<?> clazz2 = null;
        clazz2 = new Person().getClass();

        //3:直接对象调用class方法
        Class<?> clazz3 = null;
        clazz3 = Person.class;
        log.info("clazz1 name:{}",clazz1.getName());
        log.info("clazz2 name:{}",clazz2.getName());
        log.info("clazz3 name:{}",clazz3.getName());
    }


    /**
     * 3：获取父类和接口对象
     */
    @Test
    public void supperAndInterfaceTest() throws Exception{
        //1：获取父类对象
        Class<?>  clazz1 = Class.forName("com.kaigejava.domain.Student");
        Class<?>  studentParentName = clazz1.getSuperclass();
       log.info("student parentName为：{}",studentParentName);
        //2：获取接口对象
        Class<?> clazz2 =  Class.forName("com.kaigejava.service.impl.StudentServiceImpl");
        Class<?> interfaceNames[]  = clazz2.getInterfaces();
        for (int i = 0; i < interfaceNames.length; i++) {
            log.info((i + 1) + "：" + interfaceNames[i].getName());
        }
    }

    /**
     * 4：获取构造器对象
     */
    @Test
    public void getConstructorTest() throws  Exception{
        Class<?>  clazz1 = Class.forName("com.kaigejava.domain.Student");
        //1：获取所有的
        Constructor<?>[] defaultConstructors = clazz1.getConstructors();
        //循环得到每个构造器需要的参数
       for (int x = 0 ;x <defaultConstructors.length;x++){
           Constructor constructor = defaultConstructors[x];
           //获取构造器的参数
           Class<?>  [] paramTypes = constructor.getParameterTypes();
           log.info("获取到第:{}个构造器。",x);
           for (int y = 0; y < paramTypes.length; y++){
               if(y != paramTypes.length-1){
                    log.info("其中第:{}个参数为:{}",y,paramTypes[y].getName());
               }else{
                   log.info("其中第:{}个参数为:{}",y,paramTypes[y].getName()+",");
               }
           }
       }
        //2：获取有参构造器
        Class[] paramArr = new Class[]{String.class};
        Constructor<?> paramConstructor = clazz1.getConstructor(paramArr);
       Student student =  (Student)paramConstructor.newInstance("北京大学附属中学");
       log.info("使用有参构造器实例化的对象:{}",student.toString());

    }


    /**
     * 5:获取实例对象
     */
    @Test
    public void createNewObjeTest() throws  Exception{
        Class<?>  clazz1 = Class.forName("com.kaigejava.domain.Student");
        Student student = (Student)clazz1.newInstance();
        student.setAge(25);
        student.setName("凯哥Java");
        student.setSchoolName("北京大学附属中学");
        log.info("通过反射得到是student:{}",student.toString());
    }

    /**
     * 获取属性
     * 1：pubic属性获取
     * 2：所有属性获取
     */
    @Test
    public void getFieldTest() throws  Exception{
        Class<?>  clazz1 = Class.forName("com.kaigejava.domain.Student");
        //1:获取public的单个属性
        Field addrField =  clazz1.getField("addr");
        log.info("获取public addr..");
        //获取所有公开的属性
        Field[] publicFieldS = clazz1.getFields();
        for(int x = 0;x < publicFieldS.length;x++){
            Field field = publicFieldS[x];
            log.info("获取到所有pulibc的属性.当前获取第:{}个。属性名称为:{}",x,field.getName());
        }
        //获取单个属性(private)
        Field privateSchooleName = clazz1.getDeclaredField("schoolName");
        //获取所有属性 包含public private的
        Field []  allField = clazz1.getDeclaredFields();
        for(int x = 0;x < allField.length;x++){
            Field field = allField[x];
            log.info("获取到所有的属性.当前获取第:{}个。属性名称为:{}",x,field.getName());
        }
    }


    /**
     * 获取方法
     * @throws Exception
     */
    @Test
    public void getMethodTest()throws  Exception{
        Class<?>  clazz1 = Class.forName("com.kaigejava.domain.Student");
        Class<?> methodParams[] = new Class[]{String.class};
        Method publicSetRealName = clazz1.getMethod("setRealName",methodParams);
        log.info("方法名称为：{}，方法参数为：{}",publicSetRealName.getName(),publicSetRealName.getGenericParameterTypes());
    }


    /**
     * 方法调用
     */
    @Test
    public void invokeMethodTest()throws  Exception{
        //1：获取到class对象
        Class clazz1 = Class.forName("com.kaigejava.domain.Student");
        //2：获取对象实例
        Constructor<?> constructor =  clazz1.getConstructor();
        Student student = (Student)constructor.newInstance();
        Class [] methodParamArgs = new Class[]{String.class};
        //3：获取对象方法 获取到setSchoolName方法
        Method setSchoolNameMethod = clazz1.getMethod("setSchoolName",methodParamArgs);
        //4：方法调用invoke方法  set方法调用  setSchoolName方法
        setSchoolNameMethod.invoke(student,new Object[]{"北京大学附属中学"});
        log.info("获取学校名称为:{}",student.getSchoolName());
        //get方法调用
        Method getSchooleNAMEmETHOD = clazz1.getMethod("getSchoolName");
        Student getStudent = (Student)constructor.newInstance();
        getStudent.setSchoolName("清华大学附属中学");
        log.info("通过调用getSchooleName方法获取到的数据为:{}",getSchooleNAMEmETHOD.invoke(getStudent));
    }


    /**
     * 操作类的属性
     */
    @Test
    public void fieldTest()throws  Exception{
        //1：获取到class对象
        Class clazz1 = Class.forName("com.kaigejava.domain.Student");
        //2：通过class对象获取到对象实例
        Student student =(Student) clazz1.newInstance();
        //3：通过对象实例获取到需要操作的属性名称
        Field field = clazz1.getField("realName");
        //4：设置是否可以操作private属性。true:运行操作；false:不允许操作。
        field.setAccessible(false);
        //5：调用field.set(Object obj,Object value)方法给对象的属性设置新值
        field.set(student,"凯哥Java");
        //6:调用field.get(Object obj)方法，得到对象属性的值
        String getRelName = (String )field.get(student);
        log.info("通过反射设置public属性后，得到属性值为:{}",getRelName);
        //获取私有属性 并设置值
        Field schoolNameField = clazz1.getDeclaredField("schoolName");
        //此次如果设置了false。会出现如下错误：
        //java.lang.IllegalAccessException: Class com.kaigejava.ReflectDemoTest can not access a member of class com.kaigejava.domain.Student with modifiers "private"
        schoolNameField.setAccessible(true);
        schoolNameField.set(student,"清华大学附属中学");
        String getSchoolName = (String)schoolNameField.get(student);
        log.info("反射获取到private属性 schoolName的值为:{}",getSchoolName);
    }
}
