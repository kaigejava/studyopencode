package com.kaigejava;

import com.kaigejava.domain.Person;
import com.kaigejava.service.impl.PersonServiceImpl;
import com.kaigejava.service.impl.StudentServiceImpl;
import com.kaigejava.utils.ContextBeanUtils;
import com.kaigejava.utils.XmlUtils;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.core.Is;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

/**
 * Created by kaigejava on 2019/6/23.
 * spring 基于xml配置的时候，通过解析XML得到context对象
 * 在通过context的getBean方法得到需要的对象
 */
@Slf4j
public class SpringTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        Person person = (Person)context.getBean("person");
        //TODO......
    }


    @Test
    public void refSpringDemoTest(){
        //获取context
      Map<String ,Object> context =   XmlUtils.getBeanMap("application.xml");
        ContextBeanUtils<StudentServiceImpl> beanUtils = new ContextBeanUtils<>();
        StudentServiceImpl studentService = beanUtils.getBean(context,"studentService");
        log.info("调用方法:{}",studentService.getList());
        //获取person对象
        ContextBeanUtils<PersonServiceImpl> serviceContextBeanUtils = new ContextBeanUtils<>();
        PersonServiceImpl personService = serviceContextBeanUtils.getBean("persontService");
        log.info("personService.list:{}",personService.getList());
        log.info("personService.getPersonByName:{}",personService.getPersonByName("sixiaosi"));
    }

}
