package com.kaigejava;

import java.io.InputStream;
import java.util.Iterator;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * DOM4J解析XML文件
 */
public class XMLDemo1 {

    public static void main(String[] args) throws DocumentException{
        /** 1.创建一个读取XML文件的对象 用来指向XML文件的输入流
         *  这个XML文件其实就是磁盘上的一个物理文件,我需要将它变成JAVA的对象
         *  我们知道JAVA的对象是存在内存中的,所以我们就是要将物理的磁盘上的数据拿到内存中
         *  那么首先我们是要将文件的内容拿到内存中,然后再内存中进行处理
         */
        SAXReader reader = new SAXReader();

        /**
         * 2.创建一个(当前项目类路径下的)输入流 指向这个XML文件
         * 因为它不是直接将文件内容拿到内存里,而是通过读取流,所以要先创建一个输入流
         * 语法:
         * InputStream 引用名 = 类名.class.getClassLoader().getResourceAsStream("文件名");
         */
        InputStream is =
                XMLDemo1.class.getClassLoader().getResourceAsStream("application.xml");
        /**
         *3.我们通过SAXReader这个对象利用流来读取这个对象 通过这个方法我们就把所有的XML数据拿到内存里了 返回了一个文档类型的对象
         * 通过reader下的对象调用read()方法,并且将InputStream对象传进去
         * 注意：导包的时候导入import org.dom4j.Document;包
         */
        Document doc = reader.read(is);

        /**
         * 拿到XML的内容之后，首先我们要读取xml的根元素,再通过根元素读取子元素,再读取元素点里面的值
         */
        /**
         * 4.获取根元素
         * 注意：导包的时候导入import org.dom4j.Element;
         */
        Element root = doc.getRootElement();
        //System.out.println(root.getName()); //获取根元素的名字 companys

        /**
         * 5.通过根元素获取子元素的迭代器
         * 导入：
         * import java.util.Iterator;包
         * import org.dom4j.Attribute;包
         */
        Iterator<Element> it = root.elementIterator();
        while(it.hasNext()){
            Element e = it.next();//获取子元素
           // System.out.println(e.getName());


            // 获取元素
            Element idElement = e.element("id");
            String id =  idElement.getText();
            System.out.println(id);
            //通过元素对象获取子元素对象
            Element nameElement = e.element("all-class");
            //获取元素中的文本内容
            String name = nameElement.getText();
            System.out.println(name); //

        }
    }
}